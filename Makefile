run: client
	./client
client: client.o
	gcc -o client client.o -lncurses -lpthread
client.o: client.c
	gcc -std=gnu99 -c client.c
clean:
	rm client client.o