#define _SVID_SOURCE
#include <sys/types.h>
#include <ncurses.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <sys/msg.h>
#include <unistd.h> 
#include <time.h>

#include "option.h"

int lvl8_8(int sizemap, char* message, int minecount);
int randomz(char **mas, int sizemap, int minecount);
int is_mine(char **mas, int sizemap, int poz_y, int poz_x);
int move_map(int *choice_y, int *choice_x, int sizemap);
int is_null(char **mas, char **mask, int sizemap, int choice_y, int choice_x);
void* check_func(void* data);

int main(int argc, char const *argv[]){
	const char *hi_msg = "Welcome to the game!";
	const char menu[MENUCOUNT][8] = {"8x8", "16x16", "30x30", "exit"};
	int men_y = 0;
	int men_x = 0;

	initscr();	//переход в ncurses режим
	start_color();	//включаем цветной режим
	noecho();	//отключаем вывод на экран (при вводе)
	curs_set(0);
	keypad(stdscr, true);
	getmaxyx(stdscr, men_y, men_x);
	//инициализация "цветовых пар"
	init_pair (1, COLOR_YELLOW, COLOR_BLACK);
	init_pair (2, COLOR_RED, COLOR_WHITE);
	init_pair (3, COLOR_BLACK, COLOR_BLACK);
	init_pair (4, COLOR_GREEN, COLOR_BLACK);
	init_pair (5, COLOR_BLUE, COLOR_BLACK);
	init_pair (6, COLOR_MAGENTA, COLOR_BLACK);
	init_pair (7, COLOR_CYAN, COLOR_BLACK);
	init_pair (8, COLOR_WHITE, COLOR_BLACK);
	init_pair (9, COLOR_RED, COLOR_BLACK);
	init_pair (10, COLOR_RED, COLOR_RED);

	mvprintw( men_y/2, (men_x-strlen(hi_msg))/2, hi_msg);
	refresh();
	sleep(2);
	int sizemap_8 = 8;
	char* message_8 = "8x8, 10 mines";
	int minecount_8 = 10;
	int sizemap_16 = 16;
	char* message_16 = "16x16, 40 mines";
	int minecount_16 = 40;
	int sizemap_30 = 30;
	char* message_30 = "30x30, 99 mines";
	int minecount_30 = 99;

	int choice = 0;
	while(1){
		clear();
		for (int i = 0; i < MENUCOUNT; i++){
			if (i == choice){
				attron(A_BOLD);
				attron (COLOR_PAIR(1));
				mvaddch( ((men_y-MENUCOUNT)/2)+i, men_x/2-strlen(menu[0]+1),'>');
			}
			else
				mvaddch( ((men_y-MENUCOUNT)/2)+i, men_x/2-strlen(menu[0]+1),' ');
			printw("%s\n", menu[i]);
			attroff (COLOR_PAIR(1));
			attroff(A_BOLD);
		}
		switch(getch()){
			case KEY_UP:
				if (choice != 0)
					choice--;
				break;
			case KEY_DOWN:
				if (choice != MENUCOUNT-1)
					choice++;
				break;
			case '\n':
				if (choice == 0){
					/* вызвать функцию для режима 8х8 */
					lvl8_8(sizemap_8, message_8, minecount_8);
				}
				else if (choice == 1){
					lvl8_8(sizemap_16, message_16, minecount_16);
				}
				else if (choice == 2){
					lvl8_8(sizemap_30, message_30, minecount_30);
				}
				else if (choice == MENUCOUNT-1){
					endwin();
					exit(0);
				}
				break;

		}
	}
	getch();
	endwin();	//выход из ncurses режима
	return 0;
}

int lvl8_8(int sizemap, char* message, int minecount){
	char *hi_msg = message;
	int choice_y = 0;
	int choice_x = 0;
	int status = 0;
	int recvn = 0;
	struct datatype data;
	msqid = msgget (IPC_PRIVATE, IPC_CREAT | 0600);

	int row, col;
	getmaxyx(stdscr, row, col);
	
	clear();
	attron(A_BOLD);
	attron(COLOR_PAIR(1));
	mvprintw( row/2, (col-strlen(hi_msg))/2, hi_msg);
	refresh();
	attroff (COLOR_PAIR(1));
	attroff(A_BOLD);
	//массив-маска
	char **mask;
	mask = (char**)malloc(sizemap * sizeof(char*));
	for (int i = 0; i < sizemap; ++i){
		mask[i] = (char*)malloc(sizemap * sizeof(char));
	}
	//заполнение
	for (int i = 0; i < sizemap; ++i){
		memset(mask[i], 64, sizemap*sizeof(char));	//символ '@'
	}
	//массив-карта
	char **mas;
	mas = (char**)malloc(sizemap * sizeof(char*));
	for (int i = 0; i < sizemap; ++i){
		mas[i] = (char*)malloc(sizemap * sizeof(char));
	}
	randomz(mas, sizemap, minecount);	//вызов функции заполнения

	data.massiv = mask;
	data.size_map = sizemap;
	data.mine_count = minecount;
	pthread_t check_thread;
	pthread_create(&check_thread, NULL, check_func, &data);

	move((row-sizemap)/2,(col-sizemap)/2);
	sleep(1);

	while(1){
		if((msgrcv(msqid, &recvn, sizeof(recvn), 0, IPC_NOWAIT)) > 0){
			break;
		}
	}
	

	while(true){
		clear();
		//вывод маски
		for (int i = 0; i < sizemap; ++i){
			for (int j = 0; j < sizemap; ++j){
				
				if (i==choice_y && j==choice_x){
					attron (COLOR_PAIR(2));
					mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
					attroff (COLOR_PAIR(2));
				} 
				else{
					if (mask[i][j] == 48){
						attron(COLOR_PAIR(3));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(3));
					}else
					if (mask[i][j] == 49){
						attron(COLOR_PAIR(5));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(5));
					}else
					if (mask[i][j] == 50){
						attron(COLOR_PAIR(4));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(4));
					}else
					if (mask[i][j] == 51){
						attron(COLOR_PAIR(9));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(9));
					}else
					if (mask[i][j] == 52){
						attron(A_DIM);
						attron(COLOR_PAIR(5));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(5));
						attroff(A_DIM);
					}else
					if (mask[i][j] == 53){
						attron(A_DIM);
						attron(COLOR_PAIR(9));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(9));
						attroff(A_DIM);
					}else
					if ((mask[i][j] == 54)||(mask[i][j] == 55)||(mask[i][j] == 56)){
						attron(COLOR_PAIR(7));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(7));
					}else
					if (mask[i][j] == 35){
						attron(COLOR_PAIR(10));
						mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
						attroff(COLOR_PAIR(10));
					}
					else mvaddch( ((row-sizemap)/2)+i, ((col-sizemap)/2)+j, mask[i][j]);
				}
			}
		}
		
		mvprintw(row-1, 1, "	%d  mines left.", recvn);
		mvprintw(row-1, col/2,"y=%d ,x=%d", choice_y, choice_x); 
		refresh();

		pthread_mutex_lock(&mutex);
		msgrcv(msqid, &recvn, sizeof(recvn), 0, IPC_NOWAIT);
				

		//передвижение по карте
		if ((status = move_map(&choice_y, &choice_x, sizemap))>0){
			mask[choice_y][choice_x] = mas[choice_y][choice_x];
			
			if (recvn==minecount){
				//youWIN!!
				clear();
				attron(COLOR_PAIR(1));
				mvprintw( row/2, (col-strlen(hi_msg))/2, "You WIN! Congratulations!!!");
				refresh();
				break;
				attroff (COLOR_PAIR(1));
			}
			
			if (mask[choice_y][choice_x] == 35){
				attron(COLOR_PAIR(10));
				move(((row-sizemap)/2)+choice_y, (col-sizemap)/2+choice_x);
				addch(35);
				attroff(COLOR_PAIR(10));
				
				pthread_mutex_unlock(&mutex);
				mvprintw(1, (col-sizemap)/2, "Game over...");
				refresh();
				sleep(1);
				//getch();
				break;
			}
			else if (mask[choice_y][choice_x] == 48){
				is_null(mas, mask, sizemap, choice_y, choice_x);
			}
		}
		pthread_mutex_unlock(&mutex);
		
		
	}

	getch();
	//освобождение памяти
	msgctl(msqid, IPC_RMID, NULL);	//удаление очереди

	for (int i = 0; i < sizemap; ++i){
		free(mask[i]);
	}
	free(mask);

	for (int i = 0; i < sizemap; ++i){
		free(mas[i]);
	}
	free(mas);
	return 0;
}

 //ПЕРЕНЕСТИ НА СЕРВЕР!!!!
int randomz(char **mas, int sizemap, int minecount){
	srand(time(NULL));
	int mine = minecount; 
	int delta = sizemap * sizemap;
	char temp[delta];
	memset(temp, 32, delta*sizeof(char));
	int n = 0;

	for (int i = 0; i < sizemap; ++i){
		memset(mas[i], 32, sizemap*sizeof(char));	//заменить на 32(пробел)	???
	}
	//заполнение врЕменной строки
	while(1){
		if(mine != 0){
			n = rand()%delta;
			if (temp[n] != 35){
				temp[n] = 35;
				mine--;
			}
		} else break;
	}
	//перенос в исходный массив
	for (int i = 0; i < sizemap; ++i){
		for (int j = 0; j < sizemap; ++j){
			mas[i][j] = temp[sizemap*i+j];
		}
	}
	//обрисовка цифрами
	for (int i = 0; i < sizemap; ++i){
		for (int j = 0; j < sizemap; ++j){
			is_mine(mas, sizemap, i, j);
		}
	}

	return 0;
}

int is_mine(char **mas, int sizemap, int poz_y, int poz_x){
	if (mas[poz_y][poz_x] == 35){
		return 1;
	}
	int count = 48;
	for (int i = poz_y-1; i <= poz_y+1; i++){
		for (int j = poz_x-1; j <= poz_x+1; j++){
			if (i<0 || j<0 || i>=sizemap || j>=sizemap || (i==poz_y && j==poz_x)){
				continue;
			} 
			else{
				if (mas[i][j] == 35){
					count++;
				}
			}
		}
	}
	mas[poz_y][poz_x] = count;
	return 0;
}

int move_map(int *choice_y, int *choice_x, int sizemap){
	//keypad(stdscr, true);
	switch(getch()){
		case KEY_UP:
			if ((*choice_y) != 0)
				(*choice_y)--;
			break;
		case KEY_DOWN:
			if ((*choice_y) != sizemap-1)
				(*choice_y)++;
			break;
		case KEY_LEFT:
			if ((*choice_x) != 0)
				(*choice_x)--;
			break;
		case KEY_RIGHT:
			if ((*choice_x) != sizemap-1)
				(*choice_x)++;
			break;
		case '\n':
			return 1;
			break;
	}
	return -1;
}

int is_null(char **mas, char **mask, int sizemap, int choice_y, int choice_x){
	for (int i = choice_y-1; i <= choice_y+1; i++){
		for (int j = choice_x-1; j <= choice_x+1; j++){
			if (i<0 || j<0 || i>=sizemap || j>=sizemap || (i==choice_y && j==choice_x)){
				continue;
			} 
			else{
				if (mas[i][j] == 48){
					if (mask[i][j] == 64){
						mask[i][j] = mas[i][j];
						is_null(mas, mask, sizemap, i, j);
					}
				} else if (mas[i][j]!=35){
					if (mask[i][j] == 64){
						mask[i][j] = mas[i][j];
					}					
				}
			}
		}
	}
	return 0;
}

void* check_func(void* data){
	pthread_detach(pthread_self());
	unsigned int usecs = 700000;
	struct datatype data_new;
	data_new = *(datatype*)data;
	int n;
	while(1){
		pthread_mutex_lock(&mutex);
		n = 0;
		for (int i = 0; i < data_new.size_map; ++i){
			for (int j = 0; j < data_new.size_map; ++j){
				if (data_new.massiv[i][j] == 64){
					n++;
				}
				if (data_new.massiv[i][j] == 35){
					pthread_mutex_unlock(&mutex);
					return NULL;
				}
			}
		}
	//	printw("!!!!%d!!!!", n);
		msgsnd(msqid, &n, sizeof(n), 0);
		pthread_mutex_unlock(&mutex);
		if (n == data_new.mine_count){
			break;
		}
		//sleep(1);
		usleep(usecs);
	}
	return NULL;
}
